<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

if (strstr($_SERVER['SERVER_NAME'], 'labmailab.local')) {
   define('DB_NAME', 'local');
   define('DB_USER', 'root');
   define('DB_PASSWORD', 'root');
   define('DB_HOST', 'localhost');
} else {
   define('DB_NAME', 'labmailab');
   define('DB_USER', 'remote');
   define('DB_PASSWORD', 'gap35ds3');
   define('DB_HOST', 'localhost');
}

define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');


/** The name of the database for WordPress */
define('DB_NAME', 'local');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kGUhC/YOJtdbBOOhCagur/IJ1hzN2DWkkW+42MsYAvldP6NBsnjz6zLBCn+DL8RRBoE+qP09nfyX7/I7tHbGaw==');
define('SECURE_AUTH_KEY',  'is0F4crPhRRR4O9XZbOw1tK5FFg+cRh9kjaNia43FsiNo9276EoGl90KxoDDdUlEZkTlvj2wTFuHsC2hI+C+zA==');
define('LOGGED_IN_KEY',    '1reiT0er9Au7OqDffEaG2fEazdaPJOVkkT+KE0bAdMiLxLvfCkt9IacEfVXnKgJqSeWXyTWBweQeWEkoXWKRUw==');
define('NONCE_KEY',        'pitL3nR2LCn+ujX8PVYHkEnE3gj9l6AWXF6wdxOIILtegJvlNEiKmlyh8v2D0yXWEhh4oODnMdjoxi+BfG1QUA==');
define('AUTH_SALT',        'PA72LYq5jw/1EkwDBybhlrNnMXqz7YJXAv+3xtXjpaNZGOvoyugIQZ1VqWlGWdO+2RE9QYFDIYIL46t9z7o8jQ==');
define('SECURE_AUTH_SALT', '09I5o/2OypjXadVpzi6nvOAU+VH0gRXbAKSsqzAHRL9WDL9ThjCgQThuCxks/RdljZjy2XMgde/f0FjIxd5MPQ==');
define('LOGGED_IN_SALT',   'wbk1lMyPbekHZg7mhMWnVX0+VzKoGa1kvIBvMNUKfYr82jHV2eY79fyjTAFc8Cx8YqBqGDdD9yYl2UAEhhp/lw==');
define('NONCE_SALT',       'fFMii+dZndAjky5OJJMxnZty6gYAgp5QG9TINDmupDzRgFQ8TUg1mw48SDoEi7moq63RPIfIpi943s1iD/NNuw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
   define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
