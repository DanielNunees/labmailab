         <!-- ======= Footer ======= -->
         <footer id="footer">

            <div class="footer-top">
               <div class="container">
                  <div class="row">


                     <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Links Úteis</h4>
                        <ul>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Sobre</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Serviços</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Termos</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Política de privacidade</a></li>
                        </ul>
                     </div>

                     <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nossos Serviços</h4>
                        <ul>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Serviço 1</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Serviço 2</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Serviço 3</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Serviço 4</a></li>
                           <li><i class="bx bx-chevron-right"></i> <a href="#">Serviço 5</a></li>
                        </ul>
                     </div>

                     <div class="col-lg-6 col-md-6 footer-newsletter">
                        <h4>Junte-se à nossa newsletter</h4>
                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                        <form action="" method="post">
                           <input type="email" name="email"><input type="submit" value="Subscribe">
                        </form>
                     </div>

                  </div>
               </div>
            </div>

            <div class="container d-md-flex py-4">

               <div class="me-md-auto text-center text-md-start">
                  <div class="copyright">
                     &copy; Copyright <strong><span>Mailab</span></strong>. Todos os direitos reservados
                  </div>
                  <div class="credits">
                     <!-- All the links in the footer should remain intact. -->
                     <!-- You can delete the links only if you purchased the pro version. -->
                     <!-- Licensing information: https://bootstrapmade.com/license/ -->
                     <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/ -->
                     Desenvolvido por <a href="https://goalflux.com/">Goal Flux</a>
                  </div>
               </div>
               <div class="social-links text-center text-md-right pt-3 pt-md-0">
                  <a href="">
                     <box-icon name='twitter' type='logo' color="white"></box-icon></i>
                  </a>
                  <a href="">
                     <box-icon name='facebook' type='logo' color="white"></box-icon></i>
                  </a>
                  <a href="">
                     <box-icon name='instagram' type='logo' color="white"></box-icon>
                  </a>
                  <a href="">
                     <box-icon name='linkedin' type='logo' color="white"></box-icon>
                  </a>
               </div>
            </div>
         </footer><!-- End Footer -->


         <?php wp_footer(); ?>
         </body>

         </html>