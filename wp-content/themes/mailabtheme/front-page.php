<?php
get_header();
?>
<!-- Blog Page -->

<!-- Main Content -->
<?php while (have_posts()) {
   the_post() ?>

   <?php get_template_part('template-parts/front-page/banner'); ?>
   <main id="main">
      <?php get_template_part('template-parts/front-page/why-us'); ?>
      <?php get_template_part('template-parts/front-page/about'); ?>
      <?php get_template_part('template-parts/front-page/counts'); ?>
      <?php get_template_part('template-parts/front-page/services'); ?>
      <?php get_template_part('template-parts/front-page/appointment'); ?>
      <?php //get_template_part('template-parts/front-page/departments'); 
      ?>
      <?php get_template_part('template-parts/front-page/doctors'); ?>
      <?php get_template_part('template-parts/front-page/faq'); ?>
      <?php get_template_part('template-parts/front-page/testimonials'); ?>
      <?php get_template_part('template-parts/front-page/gallery'); ?>
      <?php get_template_part('template-parts/front-page/contact'); ?>

   </main>
<?php }
get_footer();
