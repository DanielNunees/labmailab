<?php

function theme_files()
{
   if (strstr($_SERVER['SERVER_NAME'], 'labmailab.local')) {
      wp_enqueue_script('mainScript-js', 'http://localhost:3000/bundled.js', NULL, '1.0', TRUE);
   } else {
      wp_enqueue_style('theme_main_styles', get_theme_file_uri('/bundled-assets/styles.d2b9ab7fca99c8c5ed7e.css'));
      wp_enqueue_script('our-vendors-js', get_theme_file_uri('/bundled-assets/vendors~scripts.8a4e9da6ead103e6eaba.js'), NULL, '1.0', TRUE);
      wp_enqueue_script('mainScript-js', get_theme_file_uri('/bundled-assets/scripts.d2b9ab7fca99c8c5ed7e.js'), NULL, '1.0', TRUE);
   }
}
add_action('wp_enqueue_scripts', 'theme_files');
