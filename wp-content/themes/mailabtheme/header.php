<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
   <meta charset="<?php bloginfo('charset') ?>" />
   <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
   <?php wp_head(); ?>
</head>

<body class="is-preload" <?php body_class(); ?>>
   <?php get_template_part('template-parts/front-page/top-bar'); ?>

   <!-- ======= Header ======= -->
   <header id="header" class="fixed-top">
      <div class="container d-flex align-items-center">

         <!-- <h1 class="logo me-auto"><a href="index.html">Medilab</a></h1> -->
         <!-- Uncomment below if you prefer to use an image logo -->
         <a href="/"><img width="60%" src="<?php echo get_theme_file_uri('images/new_logo.png') ?>" alt="" class="img-fluid"></a>

         <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
               <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
               <li><a class="nav-link scrollto" href="#about">Sobre</a></li>
               <li><a class="nav-link scrollto" href="#services">Services</a></li>
               <!-- <li><a class="nav-link scrollto" href="#departments">Departments</a></li> -->
               <li><a class="nav-link scrollto" href="#doctors">Equipe</a></li>
               <li class="dropdown">
                  <a href="#"><span>Opções</span>
                     <i class="fas fa-chevron-down"></i>
                  </a>
                  <ul>
                     <li><a href="#">Opção 1</a></li>
                     <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="fas fa-chevron-right"></i></a>
                        <ul>
                           <li><a href="#">Deep Drop Down 1</a></li>
                           <li><a href="#">Deep Drop Down 2</a></li>
                           <li><a href="#">Deep Drop Down 3</a></li>
                           <li><a href="#">Deep Drop Down 4</a></li>
                           <li><a href="#">Deep Drop Down 5</a></li>
                        </ul>
                     </li>
                     <li><a href="#">Drop Down 2</a></li>
                     <li><a href="#">Drop Down 3</a></li>
                     <li><a href="#">Drop Down 4</a></li>
                  </ul>
               </li>
               <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
            </ul>


            <box-icon class="mobile-nav-toggle" name='menu'></box-icon>


         </nav><!-- .navbar -->

         <a href="#appointment" class="appointment-btn scrollto"><span class="d-none d-md-inline">Marcar</span> Consulta</a>

      </div>
   </header><!-- End Header -->