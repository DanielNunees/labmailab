import { func } from "prop-types";

class Custom {
   constructor() {
      this.nav_bar = document.querySelector('#navbar');
      this.mobile_nav_toggle = document.querySelector('.mobile-nav-toggle');
      this.scrool_to = document.querySelectorAll('.scrollto');
      this.mobile_dropdown = document.querySelector('.navbar .dropdown > a');
      this.events();
   }

   events() {
      this.mobile_nav_toggle.addEventListener('click', () => this.toggleMobileMenu());
      this.mobile_dropdown.addEventListener('click', () => this.mobileNavDropdown());
      console.log(this.scrool_to.length)
      for (let i = 0; i < this.scrool_to.length; i++) {
         this.scrool_to[i].addEventListener('click', () => this.scroolOfset());
         console.log(this.scrool_to[i])
      }
   }

   /**
   * Mobile nav toggle
   */
   toggleMobileMenu() {
      this.nav_bar.classList.toggle('navbar-mobile')
      if (this.mobile_nav_toggle.getAttribute('name') === 'x') {
         this.mobile_nav_toggle.setAttribute('name', 'menu')
      } else {
         this.mobile_nav_toggle.setAttribute('name', 'x')
      }
   }


   /**
   * Mobile nav dropdowns activate
   */
   mobileNavDropdown() {
      if (this.mobile_dropdown.classList.contains('navbar-mobile')) {
         e.preventDefault()
         this.nextElementSibling.classList.toggle('dropdown-active')
      }
   }

   /**
   * Scrool with ofset on links with a class name .scrollto
   */
   scroolOfset() {
      console.log(this.nav_bar.classList.contains('navbar-mobile'))
      if (this.nav_bar.classList.contains('navbar-mobile')) {
         this.mobile_nav_toggle.classList.remove('navbar-mobile')
         this.toggleMobileMenu()
      }

   }


}

export default Custom;


