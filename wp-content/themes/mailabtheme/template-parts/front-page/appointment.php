<!-- ======= Appointment Section ======= -->
<section id="appointment" class="appointment section-bg">
   <div class="container">

      <div class="section-title">
         <h2>Marcar uma Consulta</h2>
         <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>

      <form action="forms/appointment.php" method="post" role="form" class="php-email-form">
         <div class="row">
            <div class="col-md-4 form-group">
               <input type="text" name="name" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Minimo quatro letras">
               <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3 mt-md-0">
               <input type="email" class="form-control" name="email" id="email" placeholder=" Email" data-rule="email" data-msg="Por favor digite um email valido">
               <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3 mt-md-0">
               <input type="tel" class="form-control" name="phone" id="phone" placeholder="Telefone" data-rule="minlen:4" data-msg="Minino quatro letras">
               <div class="validate"></div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-4 form-group mt-3">
               <input type="datetime" name="date" class="form-control datepicker" id="date" placeholder="Data da consulta" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
               <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3">
               <select name="department" id="department" class="form-select">
                  <option value="">Selecione o Departamento</option>
                  <option value="Department 1">Department 1</option>
                  <option value="Department 2">Department 2</option>
                  <option value="Department 3">Department 3</option>
               </select>
               <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3">
               <select name="doctor" id="doctor" class="form-select">
                  <option value="">Selecione</option>
                  <option value="Doctor 1">Doctor 1</option>
                  <option value="Doctor 2">Doctor 2</option>
                  <option value="Doctor 3">Doctor 3</option>
               </select>
               <div class="validate"></div>
            </div>
         </div>

         <div class="form-group mt-3">
            <textarea class="form-control" name="message" rows="5" placeholder="Menssagem (Opcional)"></textarea>
            <div class="validate"></div>
         </div>
         <div class="mb-3">
            <div class="loading">Carregando</div>
            <div class="error-message"></div>
            <div class="sent-message">A sua solicitação para a consulta foi enviada. Obrigado!</div>
         </div>
         <div class="text-center"><button type="submit">Marcar uma Consulta</button></div>
      </form>

   </div>
</section><!-- End Appointment Section -->