  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center" style="background: url('<?php echo get_theme_file_uri('images/hero-bg.jpg') ?>') top center;">
     <div class="container">
        <h1>Laboratório Mailab</h1>
        <h2>Sua segurança em análises clínicas</h2>
        <a href="#about" class="btn-get-started scrollto">Saiba Mais</a>
     </div>
  </section><!-- End Hero -->