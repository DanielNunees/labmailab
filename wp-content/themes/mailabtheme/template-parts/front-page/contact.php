<!-- ======= Contact Section ======= -->
<section id="contact" class="contact">
   <div class="container">

      <div class="section-title">
         <h2>Contato</h2>
         <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>
   </div>

   <div style="width: 100%"><iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=350&amp;hl=en&amp;q=Av.%20Vila%20Velha,%20318%20-%20Centro,%20Pedro%20Can%C3%A1rio%20-%20ES,%2029970-000,%20Brazil+(Laborat%C3%B3rio%20Mailab)&amp;t=&amp;z=17&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe><a href="https://www.maps.ie/route-planner.htm">Driving Route Planner</a></div>

   <div class="container">
      <div class="row mt-5">

         <div class="col-lg-4">
            <div class="info">
               <div class="address">
                  <i>
                     <box-icon id="map-icon" name='map'></box-icon>
                  </i>
                  <h4>Localização:</h4>
                  <p>Av. Vila Velha, 318 - Centro, Pedro Canário - ES, 29970-000
                  </p>
               </div>

               <div class="email">
                  <i>
                     <box-icon name='envelope'></box-icon>
                  </i>
                  <h4>Email:</h4>
                  <p>laboratorio_mailab@hotmail.com</p>
               </div>

               <div class="phone">
                  <i>
                     <box-icon type='solid' name='phone-call'></box-icon>
                  </i>
                  <h4>Tefone:</h4>
                  <p>27 3764-1130</p>
               </div>

            </div>

         </div>

         <div class="col-lg-8 mt-5 mt-lg-0">

            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
               <div class="row">
                  <div class="col-md-6 form-group">
                     <input type="text" name="name" class="form-control" id="name" placeholder="Nome" required>
                  </div>
                  <div class="col-md-6 form-group mt-3 mt-md-0">
                     <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                  </div>
               </div>
               <div class="form-group mt-3">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Assunto" required>
               </div>
               <div class="form-group mt-3">
                  <textarea class="form-control" name="message" rows="5" placeholder="Menssagem" required></textarea>
               </div>
               <div class="my-3">
                  <div class="loading">Carregando...</div>
                  <div class="error-message"></div>
                  <div class="sent-message">A sua mensagem foi enviada. Obrigado!</div>
               </div>
               <div class="text-center"><button type="submit">Enviar menssagem</button></div>
            </form>

         </div>

      </div>

   </div>
</section><!-- End Contact Section -->