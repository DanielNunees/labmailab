<!-- ======= Counts Section ======= -->
<section id="counts" class="counts">
   <div class="container">

      <div class="row">

         <div class="col-lg-3 col-md-6">
            <div class="count-box">
               <i>
                  <box-icon name='test-tube' color="white"></box-icon>
               </i>
               <span data-purecounter-start="0" data-purecounter-end="800" data-purecounter-duration="1" class="purecounter"></span>
               <p>Coletas Mensais</p>
            </div>
         </div>

         <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
            <div class="count-box">
               <i>
                  <box-icon type='solid' name='paste' color="white"></box-icon>
               </i>
               <span data-purecounter-start="0" data-purecounter-end="18" data-purecounter-duration="1" class="purecounter"></span>
               <p>Exames Mensais</p>
            </div>
         </div>

         <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
               <i>
                  <box-icon name='group' color="white"></box-icon>
               </i>
               <span data-purecounter-start="0" data-purecounter-end="12" data-purecounter-duration="1" class="purecounter"></span>
               <p>Convênios</p>
            </div>
         </div>

         <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
               <i>
                  <box-icon name='award' color="white"></box-icon>
               </i>
               <span data-purecounter-start="0" data-purecounter-end="150" data-purecounter-duration="1" class="purecounter"></span>
               <p>Certificações</p>
            </div>
         </div>

      </div>

   </div>
</section><!-- End Counts Section -->