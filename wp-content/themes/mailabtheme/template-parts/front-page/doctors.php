<!-- ======= Doctors Section ======= -->
<section id="doctors" class="doctors">
   <div class="container">

      <div class="section-title">
         <h2>Equipe</h2>
         <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>

      <div class="row">
         <?php
         for ($i = 1; $i <= 4; $i++) { ?>
            <div class="col-lg-6 mt-4 mt-lg-0">
               <div class="member d-flex align-items-start">

                  <div class="pic"><img src="<?php echo get_theme_file_uri('images/doctors/doctors-' . $i . '.jpg') ?>" class="img-fluid" alt=""></div>
                  <div class="member-info">
                     <h4>DR. Carlos José de Mello </h4>
                     <span>Farmacêutico e Bioquímico</span>
                     <p>CRF / ES 1270</p>
                     <div class="social">
                        <a href="">
                           <box-icon name='twitter' type='logo' color="white"></box-icon></i>
                        </a>
                        <a href="">
                           <box-icon name='facebook' type='logo' color="white"></box-icon></i>
                        </a>
                        <a href="">
                           <box-icon name='instagram' type='logo' color="white"></box-icon>
                        </a>
                        <a href="">
                           <box-icon name='linkedin' type='logo' color="white"></box-icon>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         <?php }
         ?>
      </div>
   </div>
</section><!-- End Doctors Section -->