<!-- ======= Gallery Section ======= -->
<section id="gallery" class="gallery">
   <div class="container">

      <div class="section-title">
         <h2>Galeria</h2>
         <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>
   </div>

   <div class="container-fluid">
      <div class="row no-gutters">
         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-9.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-9.jpg') ?>" alt="" class="img-fluid">
               </a>
            </div>
         </div>

         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-2.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-2.jpg')?>" alt="" class="img-fluid">
               </a>
            </div>
         </div>

         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-3.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-3.jpg') ?>" alt="" class="img-fluid">
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-4.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-4.jpg') ?>" alt="" class="img-fluid">
               </a>
            </div>
         </div>

         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-5.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-5.jpg') ?>"" alt="" class=" img-fluid">
               </a>
            </div>
         </div>

         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-6.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-6.jpg') ?>" alt="" class="img-fluid">
               </a>
            </div>
         </div>

         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-12.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-12.jpg') ?>" alt="" class="img-fluid">
               </a>
            </div>
         </div>

         <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
               <a href="<?php echo get_theme_file_uri('images/gallery/gallery-11.jpg') ?>" class="galelry-lightbox">
                  <img src="<?php echo get_theme_file_uri('images/gallery/gallery-11.jpg') ?>" alt="" class="img-fluid">
               </a>
            </div>
         </div>

      </div>

   </div>
</section><!-- End Gallery Section -->