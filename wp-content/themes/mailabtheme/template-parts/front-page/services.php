<!-- ======= Services Section ======= -->
<section id="services" class="services">
   <div class="container">

      <div class="section-title">
         <h2>Serviços</h2>
         <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>

      <div class="row">
         <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
               <div class="icon">
                  <i>
                     <box-icon name='donate-blood' type='solid' ></box-icon>
                  </i></div>
               <h4><a href="">Exame de sangue</a></h4>
               <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
         </div>

         <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
            <div class="icon-box">
               <div class="icon"><i>
                     <box-icon type='solid' name='capsule' ></box-icon>
                  </i></div>
               <h4><a href="">Exame de Urina</a></h4>
               <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
            </div>
         </div>

         <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
            <div class="icon-box">
               <div class="icon">
                  <i>
                     <box-icon type='solid' name='virus'></box-icon>
                  </i>
               </div>
               <h4><a href="">Exame de DST</a></h4>
               <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
               <div class="icon">
                  <i>
                     <box-icon name='plus-medical'></box-icon>
                  </i>
               </div>
               <h4><a href="">Nemo Enim</a></h4>
               <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
            </div>
         </div>

         <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
               <div class="icon"><i>
                     <box-icon name='plus-medical'></box-icon>
                  </i></div>
               <h4><a href="">Dele cardo</a></h4>
               <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p>
            </div>
         </div>

         <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
               <div class="icon"><i>
                     <box-icon name='plus-medical'></box-icon>
                  </i></div>
               <h4><a href="">Divera don</a></h4>
               <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
            </div>
         </div>

      </div>

   </div>
</section><!-- End Services Section -->