  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-flex align-items-center fixed-top">
     <div class="container d-flex justify-content-between">
        <div class="contact-info d-flex align-items-center">
           <i class="bi bi-envelope"></i> <a href="mailto:contact@example.com">laboratorio_mailab@hotmail.com</a>
           <i class="bi bi-phone"></i> (27) 3764-1130
        </div>
        <div class="d-none d-lg-flex social-links align-items-center">
           <a href="">
              <box-icon name='twitter' type='logo' color="#437099"></box-icon></i>
           </a>
           <a href="">
              <box-icon name='facebook' type='logo' color="#437099"></box-icon></i>
           </a>
           <a href="">
              <box-icon name='instagram' type='logo' color="#437099"></box-icon>
           </a>
           <a href="">
              <box-icon name='linkedin' type='logo' color="#437099"></box-icon>
           </a>
        </div>
     </div>
  </div>