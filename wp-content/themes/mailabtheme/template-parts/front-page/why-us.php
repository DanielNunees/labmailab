<!-- ======= Why Us Section ======= -->
<section id="why-us" class="why-us">
   <div class="container">

      <div class="row">
         <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
               <h3>Por que escolher o Mailab?</h3>
               <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
                  Asperiores dolores sed et. Tenetur quia eos. Autem tempore quibusdam vel necessitatibus optio ad corporis.
               </p>
               <div class="text-center">
                  <a href="#" class="more-btn">Leia Mais <i class="bx bx-chevron-right"></i></a>
               </div>
            </div>
         </div>
         <div class="col-lg-8 d-flex align-items-stretch">
            <div class="icon-boxes d-flex flex-column justify-content-center">
               <div class="row">
                  <div class="col-xl-4 d-flex align-items-stretch">
                     <div class="icon-box mt-4 mt-xl-0">
                        <box-icon name='receipt' color="#1977cc" size="md"></box-icon>
                        <h4>Ética com relação aos clientes internos e externos</h4>
                        <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                     </div>
                  </div>
                  <div class="col-xl-4 d-flex align-items-stretch">
                     <div class="icon-box mt-4 mt-xl-0">
                        <box-icon name='cube-alt' color="#1977cc" size="md"></box-icon>
                        <h4>Comprometimento com a excelência</h4>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                     </div>
                  </div>
                  <div class="col-xl-4 d-flex align-items-stretch">
                     <div class="icon-box mt-4 mt-xl-0">
                        <box-icon name='images' color="#1977cc" size="md"></box-icon>
                        <h4>Tratamos cada cliente como especial e único</h4>
                        <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
                     </div>
                  </div>
               </div>
            </div><!-- End .content-->
         </div>
      </div>

   </div>
</section><!-- End Why Us Section -->